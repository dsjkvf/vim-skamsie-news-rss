vim-skamsie-news-rss
====================

## About

This is a fork of [vim-headlines-news](https://github.com/skamsie/vim-news-headlines) plugin by [Skamsie](https://github.com/skamsie), which now instead of relying on NewsAPI service, simply downloads articles from the RSS feeds provided by a user.

## Why?

Because Vim is not only a text editor but may also serve as a framework, which provides a highly customizable user interface. Thus, why invent the wheel, if we already got one?

## Configuration

To configure the plugin, in your `.vimrc` set the following variables:

  - to define the RSS feeds enter those in the following manner:

        let g:news_rss_sources = [
          \ {'name': 'TechCrunch', 'url': 'http://feeds.feedburner.com/TechCrunch/'},
          \ ]

  - to set the text wrapping option (default is 125):

        let g:news_rss_wrap_text = 75

  - to set the difference between user's time zone and GMT (in order for articles to display the publishing time correctly; default 0):

        let g:news_rss_gmt_delta = 0

  - to set the time limit to display only articles that aren't older than this particular number of hours (default is 8, and in order to disable any time limits set it to 0):

        let g:news_rss_limit_delta = 8

## Usage

The plugin introduces the only command, `NewsRSS`, which upon launching will open a new buffer with the headers and links from the defined RSS feeds. There, the following hotkeys are available:

  - While at the top menu, `Enter` will navigate to the selected feed;
  - `j/k` will go to the next / previous article;
  - `J/K` will go to the next / previous feed;
  - And wile at the article, `o` will open it in the default browser;

## Hints

  - Feeds defined in `g:news_rss_sources` will be listed one after another exactly as entered;
  - The plugin also plays nicely with [junegunn's](https://github.com/junegunn/) [limelight.vim](https://github.com/junegunn/limelight.vim), and if the latter is installed, it will be launched automatically upon entering the `news-rss` buffer;
  - The plugin can also preview articles within Vim [thanks](https://github.com/jarun/googler/wiki/Terminal-Reading-Mode-or-Reader-View#using-reader) to [reader.py](https://github.com/zyocum/reader) -- and if the script is installed, pressing `p` on an article will open it in a split `news-rss-preview` buffer;
  - And finally, while being in that buffer, pressing `s` will share the article in question on Twitter (by default, this sharing mechanism uses macOS's `open` command, which can be adapted by editing `news-rss-preview.vim` file from `ftplugin` folder.

## Credits

The name of the plugin reflects the contribution of the original [author](https://github.com/skamsie) and the initial code the plugin still shares. Which may or may not change in the future.

Having said that, if you like the described approach, another fork of the [original plugin](https://github.com/skamsie/vim-news-headlines) with some bugfixes and some new features may be found [here](https://bitbucket.org/dsjkvf/vim-skamsie-news) -- and that one indeed still works with NewsAPI.
