# -*- coding: utf-8 -*-

__title__      = "vim-skamsie-news-rss"
__maintainer__ = "dsjkvf"
__email__      = "dsjkvf@gmail.com"
__copyright__  = "dsjkvf"
__credits__    = ["skamsie <alexandru.dimian@gmail.com>"]
__version__    = "1.0.3"
__status__     = "Production"


# Modules
import feedparser
import time
from datetime import datetime, timedelta
import collections
import subprocess
import sys
import textwrap
import vim
if sys.version_info >= (3, 0):
    from html.parser import HTMLParser
    unicode = bytes
    unichr = chr
else:
    from HTMLParser import HTMLParser


# Globals
SOURCE_EMPHASIS = '⁕⁕⁕'
TITLE_EMPHASIS = '> '
GROUPS = []  # feeds
MENU = []  # menu items


# Get entries from a feed
def get_feed_items(feed_url, feed_name, gmt_delta = 0, limit_by_date = False):
    '''
    Produces a list of dictionaries of FEED'S ENTRIES for a provided feed.
    All of those later (see main()):
    - will be joined in an OrderedDict as "FEED'S SHORT ID": "FEED'S ENTRIES",
    - and then put inside a list named GROUPS:
        [OrderedDict([
            ("FEED'S SHORT ID", [
                {
                    "source": {
                        "id": "FEED'S SHORT ID", "name": u"FEED'S FULL NAME"
                    },
                    "title": u"ARTICLE'S TITLE"
                    "url": u"ARTICLE'S URL",
                    "description": "",
                    "publishedAt": datetime.datetime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND),
                },
                ...
            ])
        ])]
    '''
    feed = feedparser.parse(feed_url)
    feed_entries = feed.entries
    feed_title = feed['feed']['title']
    feed_clean = []

    if limit_by_date > 0:
        time_limit = datetime.now() - timedelta(hours = limit_by_date)

    for entry in feed.entries:
        article_title = entry.title
        article_url = entry.link
        try:
            article_published = datetime.fromtimestamp(time.mktime(entry.published_parsed)) + timedelta(hours = gmt_delta)
        except:
            article_published = datetime.fromtimestamp(time.mktime(entry.updated_parsed)) + timedelta(hours = gmt_delta)
        article_description = ''
        article_source = {'id': feed_name, 'name': feed_title}

        if limit_by_date:
            if article_published > time_limit:
                feed_clean.append({'description': article_description, 'title': article_title,
                    'url': article_url, 'publishedAt': article_published,
                    'source': article_source})
        else:
            feed_clean.append({'description': article_description, 'title': article_title,
                'url': article_url, 'publishedAt': article_published,
                'source': article_source})

    feed_clean_sorted = sorted(feed_clean, key=lambda k: k['publishedAt'], reverse=True)

    return feed_clean_sorted


# Setup Vim
def buffer_setup():
    settings = (
        'noswapfile', 'buftype=nofile',
        'nonumber', 'cc=', 'conceallevel=1',
        'concealcursor=n'
    )
    for setting in settings:
        vim.command('setlocal ' + setting)
    for token in ('(', ')', '-', '$'):
        vim.command('setlocal isk+=%s' % token)


# Setup boundaries
def first_group_ends_at(menu, title):
    for l, c in enumerate(menu):
        found = c.lower().find(title.lower())
        if found != -1:
            loc = (l + 1, found + len(title) + 1)
            return loc


# Print to Vim
def write_to_buff(s):
    """Write string to current buffer."""
    current_buff = vim.current.buffer

    # buffer.append() cannot accept unicode type,
    # must first encode to an UTF-8 string
    if isinstance(s, unicode):
        s = s.encode('utf-8', errors='replace')
    else:
        current_buff.append(s)


# Open an entry in a browser

# get an entry's url from the listing
def get_url(line_nr):
    line_number = line_nr - 1
    line = vim.current.buffer[line_number]
    if not line:
        return
    if line.startswith('[http'):
        inc = 1
        while True:
            if line[-1] == ']':
                return line[1:-1]
            line += vim.current.buffer[line_number + inc]
            inc += 1
    return get_url(line_nr + 1)

# open an entry in a default browser
def open_link():
    current_line_nr, _ = vim.current.window.cursor
    url = get_url(current_line_nr)

    if url:
        subprocess.call(['open', url])

# open an entry in the reader script within Vim
def open_link_inside():
    current_line_nr, _ = vim.current.window.cursor
    url = get_url(current_line_nr)

    if url:
        vim.command("new +set\ ft=news-rss-preview")
        vim.command("read !reader '" + str(url) + "'")
        vim.command("normal! gg")


# Navigation
def go_next_article():
    vim.command("let search_org = @/")
    vim.command(r"let @/ = ')\n> '")
    vim.command("normal! nj^mh")
    vim.command("let @/ = '^⁕⁕⁕'")
    vim.command("normal! N")
    vim.command("let source_name = getline('.')")
    vim.command("normal! 'hzz")
    vim.command("redraw")
    vim.command("echo source_name")
    vim.command("let @/ = search_org")
def go_prev_article():
    vim.command("let search_org = @/")
    vim.command(r"let @/ = ')\n> '")
    vim.command("normal! NNj^mh")
    vim.command("let @/ = '^⁕⁕⁕'")
    vim.command("normal! N")
    vim.command("let source_name = getline('.')")
    vim.command("normal! 'hzz")
    vim.command("redraw")
    vim.command("echo source_name")
    vim.command("let @/ = search_org")
def go_next_source():
    vim.command("let search_org = @/")
    vim.command("let @/ = '^⁕⁕⁕'")
    vim.command("normal! n")
    vim.command("let source_name = getline('.')")
    vim.command("normal! ^zz")
    vim.command("redraw")
    vim.command("echo source_name")
    vim.command("let @/ = search_org")
def go_prev_source():
    vim.command("let search_org = @/")
    vim.command("let @/ = '^⁕⁕⁕'")
    vim.command("normal! N")
    vim.command("let source_name = getline('.')")
    vim.command("normal! ^zz")
    vim.command("redraw")
    vim.command("echo source_name")
    vim.command("let @/ = search_org")
def go_source():
    vim.command("let search_org = @/")
    vim.command("let @/ = '⁕⁕⁕ ' . expand('<cWORD>') . ' ⁕⁕⁕'")
    vim.command("normal! n")
    vim.command("let source_name = getline('.')")
    vim.command("normal! ^zz")
    vim.command("redraw")
    vim.command("echo source_name")
    vim.command("let @/ = search_org")


def main():
    buffer_setup()

    sources = vim.eval('g:news_rss_sources')
    wrap_text = int(vim.eval('g:news_rss_wrap_text'))
    show_published_at = int(vim.eval('g:news_rss_show_published_at'))
    gmt_delta = int(vim.eval('g:news_rss_gmt_delta'))
    try:
        limit_delta = int(vim.eval('g:news_rss_limit_delta'))
    except:
        limit_delta = False

    if not sources:
        write_to_buff('Nothing to show, check your settings fisrt')
        return

    html = HTMLParser()

    col = collections.OrderedDict()
    for i in sources:
        key = i['name']
        value = i['url']
        try:
            col[key] = get_feed_items(value, key, gmt_delta = gmt_delta, limit_by_date = limit_delta)
        except:
            col[key] = [{"title": "THE FEED HAS FAILED TO RETURN ANY DATA",
                         "publishedAt": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                         "description": "",
                         "url": value}]
    GROUPS.append(col)

    wrapper = textwrap.TextWrapper(width=wrap_text)
    menu_wrapper = textwrap.TextWrapper(
        width=wrap_text, break_on_hyphens=False,
        break_long_words=False
    )
    title_wrapper = textwrap.TextWrapper(
        width=wrap_text, initial_indent=TITLE_EMPHASIS,
        subsequent_indent=TITLE_EMPHASIS
    )

    def write_content(groups):
        for group in groups:
            for title, headlines in group.items():
                write_to_buff(
                    '%s %s %s' % (
                        SOURCE_EMPHASIS,
                        title.upper(),
                        SOURCE_EMPHASIS
                    )
                )
                write_to_buff('')
                if not headlines:
                    continue
                for h in headlines:
                    if show_published_at and h['publishedAt']:
                        published_at = '(%s)' % h['publishedAt']
                    else:
                        published_at = ''
                    if published_at:
                        write_to_buff(str(published_at))
                    write_to_buff(title_wrapper.wrap(html.unescape(h['title'])))
#                     write_to_buff(title_wrapper.wrap(html.unescape(h['title'] + published_at)))
                    if h['description']:
                        write_to_buff(
                            wrapper.wrap(html.unescape(h['description'].lstrip(TITLE_EMPHASIS)))
                        )
                    write_to_buff(wrapper.wrap('[' + html.unescape(h['url'] + ']')))
                    write_to_buff('')

    def write_menu(groups, menu):
        to_write = []
        for group in groups:
            to_write.append('   '.join(map(lambda x: x.upper(), group.keys())))
        menu.extend(menu_wrapper.wrap('   '.join(to_write)))
        write_to_buff(menu)
        write_to_buff('')

    def set_buffer_vars(groups, menu):
        """Set wars used for syntax highlighting."""
        t_start_l = 1
        t_start_c = 1

        if sources:
            s_end = first_group_ends_at(menu, list(groups[0].keys())[-1])
            if s_end:
                t_start_l = s_end[0]
                t_start_c = s_end[1] + 1
        vim.command('let b:menu_topics_start_l=%s' % t_start_l)
        vim.command('let b:menu_topics_start_c=%s' % t_start_c)
        vim.command('let b:menu_until_l=%s' % (len(menu) + 1))

    write_menu(GROUPS, MENU)
    write_content(GROUPS)
    set_buffer_vars(GROUPS, MENU)

    del vim.current.buffer[0]
