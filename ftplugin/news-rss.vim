
" DESCRIPTION:  filetype plugin
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>
" CREDITS:      skamsie <alexandru.dimian@gmail.com>

" Define version independent Python launcher
if has('python')
    command! -nargs=1 Python python <args>
elseif has('python3')
    command! -nargs=1 Python python3 <args>
else
    echo "Newsrss.vim ERROR: Requires Vim compiled with +python or +python3"
    finish
endif

" Import Python code
execute "Python import sys"
execute "Python sys.path.append(r'" . expand("<sfile>:p:h") . "')"

Python << EOF
if 'news_rss' not in sys.modules:
    import news_rss
else:
    import imp
    # Reload python module to avoid errors when updating plugin
    news_rss = imp.reload(news_rss)
EOF

" Start everything
execute "Python news_rss.main()"

" Local settings
" hide line numbers
setlocal norelativenumber
setlocal nonumber
" autocommands to turn Limelight on and off
function! LimelightOn()
    try
        Limelight 0.7
    catch /^Vim\%((\a\+)\)\=:E492/
    endtry
endfunction
autocmd! BufEnter,WinEnter,TabEnter <buffer> call LimelightOn()
function! LimelightOff()
    try
        Limelight!
    catch /^Vim\%((\a\+)\)\=:E492/
    endtry
endfunction
autocmd! BufLeave,BufDelete <buffer> call LimelightOff()

" Local mappings
" go top
nnoremap <silent> <buffer> gg :echo ""<CR>gg

" go to subscription
noremap <silent> <buffer> <CR> :Python news_rss.go_source()<CR>

" next / previous article
nnoremap <silent> <buffer> j :Python news_rss.go_next_article()<CR>
nnoremap <silent> <buffer> k :Python news_rss.go_prev_article()<CR>

" next / previous subscription
nnoremap <silent> <buffer> J :Python news_rss.go_next_source()<CR>
nnoremap <silent> <buffer> K :Python news_rss.go_prev_source()<CR>

" open link
noremap <silent> <buffer> o :Python news_rss.open_link()<cr>
noremap <silent> <buffer> p :Python news_rss.open_link_inside()<cr>
