
" DESCRIPTION:  main plugin
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>
" CREDITS:      skamsie <alexandru.dimian@gmail.com>

" Avoid loading twice
if exists("g:loaded_news_rss")
    finish
endif
let g:loaded_news_rss = 1

" Filetype plugins need to be enabled
filetype plugin on

" Load ftplugin when opening .news-headlines buffer
au! BufRead,BufNewFile *.news-rss set filetype=news-rss

" Set required defaults
if !exists("g:news_rss_sources")
  let g:news_rss_sources = [{'name': 'TC', 'url': 'http://feeds.feedburner.com/TechCrunch/'}]
endif

if !exists("g:news_rss_wrap_text")
  let g:news_rss_wrap_text = 125
endif

if !exists("g:news_rss_show_published_at")
  let g:news_rss_show_published_at = 0
endif

if !exists("g:news_rss_gmt_delta")
  let g:news_rss_gmt_delta = 0
endif

if !exists("g:news_rss_limit_delta")
  let g:news_rss_limit_delta = 8
endif

function! NewsRSS(...)
  setlocal modifiable
  execute "edit .news-rss"
  normal! gg
  setlocal noma
endfunction

command! NewsRSS call NewsRSS()
